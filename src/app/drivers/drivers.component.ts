import { Component, OnInit } from '@angular/core';
import { CrudService } from './../crud/crud.service';
import { Shape, TableView, TabAction, TableRow, TableColumn, ButtonColor, TextAlign, TextColor, ActionType } from './../crud/types.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-drivers',
  templateUrl: './drivers.component.html',
  styleUrls: ['./drivers.component.scss'],
})
export class DriversComponent implements OnInit {

  actions: TabAction[];
  table: TableView;
  private rows: TableRow[];
  private columns: TableColumn[];

  constructor(private crud: CrudService) {
    // this.crud.get('https://jibrila.herokuapp.com/api/drivers').subscribe(
    //   data => console.log(data)
    // );
  }

  ngOnInit() {
    this.actions = [
      {
        icon: 'ui-1_simple-add',
        text: 'Create Driver',
        buttonShape: Shape.RECTANGLE,
        buttonColor: ButtonColor.PRIMARY,
        buttonAction: () => alert('Create Driver'),
      },
    ];

    this.columns = [
      {
        text: 'Mohammed Odunayo Fatai',
        type: ActionType.LINK,
        link: '/drivers/id',
      },
      {
        text: 'Male',
      },
      {
        text: '+2347066780373',
        align: TextAlign.RIGHT,
      },
    ];

    this.rows = [
      {
        columns: this.columns,
      },
      {
        columns: this.columns,
      },
    ];

    this.table = {
      title: 'Drivers',
      headColor: TextColor.PRIMARY,
      head: [
        {
          text: 'Name',
        },
        {
          text: 'Gender',
        },
        {
          text: 'Phone Number',
          align: TextAlign.RIGHT,
        },
      ],
      body: this.rows,
    };
  }

}
