import { Component, OnInit, Input } from '@angular/core';
import { TabAction, TableView } from '../types.service';

@Component({
  selector: 'app-read',
  templateUrl: './read.component.html',
  styleUrls: ['./read.component.scss']
})
export class ReadComponent implements OnInit {

  @Input() actions: TabAction[];
  @Input() table: TableView;

  constructor() { }

  ngOnInit() { }

}
