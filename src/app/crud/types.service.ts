import { Injectable } from '@angular/core';

export enum Shape {
  RECTANGLE = 0,
  ROUNDED = 1,
  CIRCLE = 2,
}
export enum ButtonColor {
  DEFAULT = 'btn-default',
  PRIMARY = 'btn-primary',
  INFO = 'btn-info',
  SUCCESS = 'btn-success',
  WARNING = 'btn-warning',
  DANGER = 'btn-danger',
  NEUTRAL = 'btn-neutral',
}
export enum TextColor {
  DEFAULT = 'text-default',
  PRIMARY = 'text-primary',
  INFO = 'text-info',
  SUCCESS = 'text-success',
  WARNING = 'text-warning',
  DANGER = 'text-danger',
  MUTED = 'text-muted',
}
export declare interface TabAction {
  icon: String;
  text: String;
  buttonShape: Shape;
  buttonColor: ButtonColor;
  buttonAction: Function;
}
export enum TextAlign {
  LEFT = 'text-left',
  RIGHT = 'text-right',
  CENTER = 'text-center',
}
export enum ActionType {
  TEXT = 'text',
  LINK = 'link',
  BUTTON = 'button',
}
export declare interface TableHead {
  text: String;
  align?: TextAlign;
}
export declare interface TableColumn {
  text: String;
  align?: TextAlign;
  type?: ActionType;
  link?: String;
  action?: Function;
}
export declare interface TableRow {
  columns: TableColumn[];
}
export declare interface TableView {
  title: String;
  headColor: TextColor;
  head: TableHead[];
  body: TableRow[];
}

@Injectable({
  providedIn: 'root'
})

export class TypesService {

  constructor() { }
}
