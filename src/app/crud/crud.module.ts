import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CreateComponent } from './create/create.component';
import { ReadComponent } from './read/read.component';
import { UpdateComponent } from './update/update.component';
import { DeleteComponent } from './delete/delete.component';
import { DynamicContentsModule } from './../dynamic-contents/dynamic-contents.module';

@NgModule({
  declarations: [
    CreateComponent,
    ReadComponent,
    UpdateComponent,
    DeleteComponent,
  ],
  imports: [
    CommonModule,
    DynamicContentsModule,
  ],
  exports: [
    CreateComponent,
    ReadComponent,
    UpdateComponent,
    DeleteComponent,
  ]
})
export class CrudModule { }
