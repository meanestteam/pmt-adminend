import { Component, OnInit, Input } from '@angular/core';
import { TabAction } from './../../crud/types.service';

@Component({
  selector: 'app-action-tab',
  templateUrl: './action-tab.component.html',
  styleUrls: ['./action-tab.component.scss']
})

export class ActionTabComponent implements OnInit {

  @Input() actions: TabAction[];

  constructor() { }

  ngOnInit() {
    // this.actions = [
    //   {
    //     icon: 'ui-1_simple-add',
    //     text: 'Action Name',
    //     buttonShape: Shape.RECTANGLE,
    //     buttonColor: Color.PRIMARY,
    //     buttonAction: () => alert('Onclick Action Event'),
    //   },
    //   {
    //     icon: 'ui-1_simple-add',
    //     text: 'Action Name',
    //     buttonShape: Shape.ROUNDED,
    //     buttonColor: Color.INFO,
    //     buttonAction: () => alert('Onclick Action Event'),
    //   },
    //   {
    //     icon: 'ui-1_simple-add',
    //     text: 'Action Name',
    //     buttonShape: Shape.CIRCLE,
    //     buttonColor: Color.SUCCESS,
    //     buttonAction: () => alert('Onclick Action Event'),
    //   },
    // ];
  }

}
