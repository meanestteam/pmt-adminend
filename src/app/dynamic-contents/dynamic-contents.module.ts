import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ActionTabComponent } from './action-tab/action-tab.component';
import { TableViewComponent } from './table-view/table-view.component';

@NgModule({
  declarations: [
    ActionTabComponent,
    TableViewComponent,
  ],
  imports: [
    CommonModule,
    RouterModule,
  ],
  exports: [
    ActionTabComponent,
    TableViewComponent,
  ]
})
export class DynamicContentsModule { }
