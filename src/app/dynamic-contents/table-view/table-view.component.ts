import { Component, OnInit, Input } from '@angular/core';
import { TableView } from '../../crud/types.service';
import {NgbDropdownConfig} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-table-view',
  templateUrl: './table-view.component.html',
  styleUrls: ['./table-view.component.scss'],
  providers: [NgbDropdownConfig],
})
export class TableViewComponent implements OnInit {

  @Input() table: TableView;

  constructor(config: NgbDropdownConfig) {
    config.placement = 'bottom-right';
    config.autoClose = false;
  }

  ngOnInit() { }

}
